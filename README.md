# FRONT END APPLICATION ANGULAR

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

##SETTING UP ENVIRONMENT

Run `npm install`  to install necessary modules

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## NB

Ensure that you run the backend system before testing the front end

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

