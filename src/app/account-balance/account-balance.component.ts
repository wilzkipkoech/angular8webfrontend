import { Component, OnInit } from '@angular/core';
import { BalanceDataService } from '../services/account-balance.service'
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


export class Balance {
  constructor(
    public  account_no : string,
    public balance: number
    ) {

  }
}
@Component({
  selector: 'app-account-balance',
  templateUrl: './account-balance.component.html',
  styleUrls: ['./account-balance.component.scss']
})
export class AccountBalanceComponent implements OnInit {

  account_balances: Balance[]

  balance : Balance[]



  constructor(private service: BalanceDataService,
   private router: Router,
    private toastr: ToastrService) { }

    ngOnInit() {
      this.retrieveAccountBalance();
    }

    retrieveAccountBalance() {
      this.service.retrieveAccountBalance().subscribe(
        response => {
           console.log(response);
          this.account_balances = response;
        }
      );
    }



    getBalance( accountNo: any) {
      this.service.getAccountBalance(accountNo)
        .subscribe(
          response => {
            console.log(response);
           this.balance = response;
         }
       );
    }

}
